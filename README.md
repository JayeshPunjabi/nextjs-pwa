Clone it and

```bash
npm install
```

### For Development

```bash
npm run dev
```
### For Service Worker to work

```bash
npm run build
npm run dev
```

### For Production

```bash
npm run build
npm start
```
