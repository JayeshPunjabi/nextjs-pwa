import Document, { Head, Main, NextScript } from 'next/document';

export default class CustomDocument extends Document {
  render() {
    return (
      <html lang="en">
        <Head />
        <body>
          <Main />
          <NextScript />
          <script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-app.js"></script>

          <script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-auth.js"></script>
          <script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-firestore.js"></script>
        </body>
      </html>
    );
  }
}
